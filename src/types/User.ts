type Gender = 'Male' | 'Female' | 'others'
type Role = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender // male female others
  roles: Role[] //admin user
}

export type {Gender,Role,User}
